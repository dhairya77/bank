/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package bank;

/**
 *
 * @author DHAIRYA
 */
public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
