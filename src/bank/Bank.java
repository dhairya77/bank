/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package bank;

/**
 *
 * @author DHAIRYA
 */
public class Bank {

    private String name;

    public Bank(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
